import requests


def call_server():
    headers = {'Accept': 'text/html'}
    resp = requests.get("http://integrationtestdemo_nginxserver_1:80", headers=headers)
    print(resp.status_code)
    return resp.status_code
