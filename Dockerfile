FROM python:3.7

COPY . ./app
WORKDIR /app
RUN ls
RUN pip install -r requirements-test.txt
RUN pip freeze

